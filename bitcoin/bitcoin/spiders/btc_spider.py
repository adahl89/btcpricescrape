import scrapy
import argparse
from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from scrapy_splash import SplashRequest
from scrapy.utils.log import configure_logging


class BTCSpider(scrapy.Spider):
    name = "coinprices"
    start_urls = ["https://www.cryptocompare.com/coins/#/usd"]
    max_coins = 10

    def start_requests(self):
        for url in self.start_urls:
            yield SplashRequest(url, self.parse,
                                endpoint='render.html',
                                args={'wait': 5},
                                )

    def parse(self, response):
        main_url = response.url.replace("#/usd", "")
        for tr in response.xpath('//tr[contains(@class, "ng-scope") and td]'):
            td = tr.xpath('./td[contains(@class, "place")]')
            place = td.xpath('./div/text()').extract_first()
            print 'Place:', place
            url = td.xpath('./@data-href').extract_first()
            print 'url:', url
            coin = url.split("/")[2]
            if 1 <= int(place) <= 10:
                print 'coin:', coin
                next_url = main_url + coin + '/markets/USD'
                yield SplashRequest(next_url, self.parse_coin,
                                    endpoint='render.html',
                                    args={'wait': 5},
                                    )

    def parse_coin(self, response):
        coin = response.url.split("/")[-3]
        market_prices = {coin:[]}
        for tr in response.xpath('//tr[@class="ng-scope" and td]'):
            market = tr.xpath('.//td[contains(@class, "current-market")]//span[@ng-if="pagetype==\'Coin\'"]/text()').extract_first()
            current_price = tr.xpath('./td[@class="current-price"]/div[contains(@class, "current-price-value")]/@title').extract_first()
            open_price = tr.xpath('./td[contains(@class, "current-open-24hour")]/div[@class="ng-binding"]/@title').extract_first()
            low, high = tr.xpath('./td[contains(@class, "current-high-low")]/div/@title').extract()
            market_prices[coin].append({market: [current_price, open_price, low, high]})
        yield market_prices


if __name__ == '__main__':
    CMD_OUTPUT_FILE = '-o'

    parser = argparse.ArgumentParser(description='Scrape prices of different cryptocurrencies.')
    parser.add_argument(CMD_OUTPUT_FILE, default='result.json', dest='output_file',
                        help='Output JSON file for scraping results.')

    namespace = parser.parse_args()

    settings = get_project_settings()
    settings.set('FEED_FORMAT', 'json')
    settings.set('FEED_URI', namespace.output_file)

    configure_logging()
    runner = CrawlerRunner(settings)
    runner.crawl(BTCSpider)
    d = runner.join()
    reactor.run()
