# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Scrape Top 10 cryptocurrencies prices from up to 20+ markets obtaining current price, open, low and high prices within 24 hs.
* 0.1

### How do I get set up? ###

* Must have Python 2.7 setup
  Under a virtualenv, 
      pip install scrapy
      pip install scrapy-splash
  Install docker (follow instructions in [Link Text]https://www.docker.com/community-edition#/download(Link URL)
  Install Splash with docker: docker pull scrapinghub/splash
 
* Configuration
  docker run -p 5023:5023 -p 8050:8050 -p 8051:8051 scrapinghub/splash
* Dependencies:
  Python 2.7
  Scrapy
  Scrapy-Splash
  Docker
  Splash
* Deployment instructions
  Run python btc_spider.py. It will generate a results.json file in the same directory of the script. You can indicate output file with -o flag.